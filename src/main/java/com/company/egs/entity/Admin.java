package com.company.egs.entity;

import com.company.egs.entity.enumPack.Role;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Getter
@Setter
@Entity
@Table
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false, updatable = false, insertable = false)
    private Long id;

    private String name;
    private String surname;

    @Enumerated(EnumType.STRING)
    private Role role;

}
