package com.company.egs.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
@Data
@Getter
@Setter
@Entity
@Table(name = "test")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false, updatable = false,insertable = false)
    private Long id;

    @Column(name = "questionCount",nullable = false, length = 20)
    private int questionCount;

    @JoinColumn(name = "question_id",nullable = false)
    @ManyToOne()
    private Question question;

    @JoinColumn(name = "admin_id",nullable = false)
    @ManyToOne()
    private Admin admin;

    @OneToMany(mappedBy = "test", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserTest> userTests;
}
