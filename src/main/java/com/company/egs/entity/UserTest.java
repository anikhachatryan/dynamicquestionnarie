package com.company.egs.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Data
@Getter
@Setter
@Entity
@Table(name = "user_test")
public class UserTest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false, updatable = false,insertable = false)
    private Long id;

    @JoinColumn(name = "user_id",nullable = false)
    @ManyToOne()
    private User user;

    @JoinColumn(name = "test_id",nullable = false)
    @ManyToOne()
    private Test test;

    @Column(name = "result",nullable = false)
    private int result;

    @Temporal(TemporalType.DATE)
    @Column(name = "startDate",nullable = false)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "endDate",nullable = false)
    private Date endDate;
}
