package com.company.egs.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Getter
@Setter
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false, updatable = false,insertable = false)
    private Long id;

    @Column(name = "name",nullable = false, length = 50)
    private String name;

    @Column(name = "surname",nullable = false, length = 70)
    private String surname;

    @Column(name = "email",nullable = false, length = 70, unique = true)
    private String email;

    @Column(name = "password",nullable = false, length = 50)
    private String password;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserTest> testUsers = new ArrayList<>();
}
