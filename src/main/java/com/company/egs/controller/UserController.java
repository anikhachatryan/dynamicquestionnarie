package com.company.egs.controller;

import com.company.egs.entity.User;
import com.company.egs.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/users")
public class UserController {
    private static final Logger logger = Logger.getLogger(User.class.getName());

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/")
    public ModelAndView welcome(){
        ModelAndView model = new ModelAndView();
        model.setViewName("welcome");
        return model;
    }

@RequestMapping(value = "/")
    public ModelAndView listUser(ModelAndView model) {
        List<User> users = userService.getAll();
        model.addObject("users", users);
        model.setViewName("home");
        logger.info("Getting userlist");
        return model;
    }


    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    public ModelAndView newUser(ModelAndView model) {
        User user = new User();
        model.addObject("user", user);
        model.setViewName("UserForm");
        logger.info("Adding new user");
        return model;
    }
/*
@RequestMapping(value = "/newUser/{user}", method = RequestMethod.GET)
        public User newUser(@PathVariable User user) {
           User user = new User();
            model.addObject("user", user);
            model.setViewName("UserForm");
            logger.info("Adding new user");
            return model;
           userService.add(user);
           return user;
    }*/

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ModelAndView saveUser(@ModelAttribute User user) {
        if (user.getId() == 0) {
            userService.add(user);
        } else {
            userService.update(user);
        }
        logger.info("Saving user");
        return new ModelAndView("redirect:/users");
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public ModelAndView deleteEmployee(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("id"));
        userService.delete(userId);
        logger.info("Deleting user");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public ModelAndView editUser(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("id"));
        User user = userService.get(userId);
        ModelAndView model = new ModelAndView("UserForm");
        model.addObject("user", user);
        logger.info("Editing user");
        return model;
    }
}
