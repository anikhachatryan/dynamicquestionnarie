package com.company.egs.service.interfaces;

import com.company.egs.entity.User;

import java.util.List;

public interface UserService {

    void add(User user);

    void delete(int id);

    List<User> getAll();

    User update(User user);

    User get(int id);

    int get(String email);
}
