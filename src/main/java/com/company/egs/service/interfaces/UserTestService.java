package com.company.egs.service.interfaces;

import com.company.egs.entity.UserTest;

import java.util.List;

public interface UserTestService {

    void add(UserTest userTest);

    void delete(int id);

    List<UserTest> getAll();

    UserTest update(UserTest userTest);

    UserTest get(int id);
}
