package com.company.egs.service.interfaces;

import com.company.egs.entity.Question;

import java.util.List;

public interface QuestionService {

    void add(Question question);

    void delete(int id);

    List<Question> getAll();

    Question update(Question question);

    Question get(int id);
}
