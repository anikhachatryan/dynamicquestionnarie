package com.company.egs.service.implementation;

import com.company.egs.dao.interfaces.TestDAO;
import com.company.egs.entity.Test;
import com.company.egs.service.interfaces.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TestServiceImp implements TestService {
    @Autowired
    private TestDAO testDAO;

    @Override
    @Transactional
    public void add(Test test) {
        testDAO.add(test);
    }

    @Override
    @Transactional
    public void delete(int id) {
        testDAO.delete(id);
    }

    @Override
    @Transactional
    public List<Test> getAll() {
        return testDAO.getAll();
    }

    @Override
    @Transactional
    public Test update(Test admin) {
        return testDAO.update(admin);
    }

    @Override
    @Transactional
    public Test get(int id) {
        return testDAO.get(id);
    }

    public void setTestDAO(TestDAO testDAO) {
        this.testDAO = testDAO;
    }
}
