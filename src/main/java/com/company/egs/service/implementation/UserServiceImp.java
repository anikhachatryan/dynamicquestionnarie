package com.company.egs.service.implementation;

import com.company.egs.dao.interfaces.UserDAO;
import com.company.egs.entity.User;
import com.company.egs.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public void add(User user) {
        userDAO.add(user);
    }

    @Override
    @Transactional
    public void delete(int id) {
        userDAO.delete(id);
    }

    @Override
    @Transactional
    public List<User> getAll() {
        return userDAO.getAll();
    }

    @Override
    @Transactional
    public User update(User user) {
        return userDAO.update(user);
    }

    @Override
    @Transactional
    public User get(int id) {
        return userDAO.get(id);
    }

    @Override
    public int get(String email) {
        return userDAO.get(email);
    }

    public void setUserDAO(UserDAO userDAO){
        this.userDAO = userDAO;
    }

}
