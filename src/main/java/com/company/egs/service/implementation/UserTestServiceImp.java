package com.company.egs.service.implementation;

import com.company.egs.dao.interfaces.UserTestDAO;
import com.company.egs.entity.UserTest;
import com.company.egs.service.interfaces.UserTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserTestServiceImp  implements UserTestService {
    @Autowired
    private UserTestDAO userTestDAO;

    @Override
    @Transactional
    public void add(UserTest userTest) {
        userTestDAO.add(userTest);
    }

    @Override
    @Transactional
    public void delete(int id) {
        userTestDAO.delete(id);
    }

    @Override
    @Transactional
    public List<UserTest> getAll() {
        return userTestDAO.getAll();
    }

    @Override
    @Transactional
    public UserTest update(UserTest userTest) {
        return userTestDAO.update(userTest);
    }

    @Override
    @Transactional
    public UserTest get(int id) {
        return userTestDAO.get(id);
    }

    public void setUserTestDAO(UserTestDAO userTestDAO) {
        this.userTestDAO = userTestDAO;
    }
}
