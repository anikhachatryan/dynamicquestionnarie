package com.company.egs.service.implementation;

import com.company.egs.dao.interfaces.QuestionDAO;
import com.company.egs.entity.Question;
import com.company.egs.service.interfaces.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class QuestionServiceImp implements QuestionService {
    @Autowired
    private QuestionDAO questionDAO;

    @Override
    @Transactional
    public void add(Question question) {
        questionDAO.add(question);
    }

    @Override
    @Transactional
    public void delete(int id) {
        questionDAO.delete(id);
    }

    @Override
    @Transactional
    public List<Question> getAll() {
        return questionDAO.getAll();
    }

    @Override
    @Transactional
    public Question update(Question question) {
        return questionDAO.update(question);
    }

    @Override
    @Transactional
    public Question get(int id) {
        return questionDAO.get(id);
    }

    public void setQuestionDAO(QuestionDAO questionDAO) {
        this.questionDAO = questionDAO;
    }
}
