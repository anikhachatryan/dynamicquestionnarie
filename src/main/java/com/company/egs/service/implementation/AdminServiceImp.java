package com.company.egs.service.implementation;

import com.company.egs.dao.interfaces.AdminDAO;
import com.company.egs.entity.Admin;
import com.company.egs.service.interfaces.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AdminServiceImp implements AdminService {
    @Autowired
    private AdminDAO adminDAO;

    @Override
    @Transactional
    public void add(Admin admin) {
        adminDAO.add(admin);
    }

    @Override
    @Transactional
    public void delete(int id) {
        adminDAO.delete(id);
    }

    @Override
    @Transactional
    public List<Admin> getAll() {
        return adminDAO.getAll();
    }

    @Override
    @Transactional
    public Admin update(Admin admin) {
        return adminDAO.update(admin);
    }

    @Override
    @Transactional
    public Admin get(int id) {
        return adminDAO.get(id);
    }

    public void setAdminDAO(AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

}
