package com.company.egs.dao.implementation;

import com.company.egs.dao.interfaces.QuestionDAO;
import com.company.egs.entity.Question;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionDAOImp implements QuestionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(Question question) {
        sessionFactory.getCurrentSession().saveOrUpdate(question);
    }

    @Override
    public void delete(int id) {
        Question question = (Question) sessionFactory.getCurrentSession().load(Question.class, id);
        if (question != null) {
            this.sessionFactory.getCurrentSession().delete(question);
        }
    }

    @Override
    public List<Question> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Question").list();
    }

    @Override
    public Question update(Question question) {
        sessionFactory.getCurrentSession().update(question);
        return question;
    }

    @Override
    public Question get(int id) {
        return (Question) sessionFactory.getCurrentSession().get(Question.class, id);
    }
}
