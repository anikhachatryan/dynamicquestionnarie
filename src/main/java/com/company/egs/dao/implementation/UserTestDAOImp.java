package com.company.egs.dao.implementation;

import com.company.egs.dao.interfaces.UserTestDAO;
import com.company.egs.entity.UserTest;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserTestDAOImp implements UserTestDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(UserTest userTest) {
        sessionFactory.getCurrentSession().saveOrUpdate(userTest);
    }

    @Override
    public void delete(int id) {
        UserTest userTest = (UserTest) sessionFactory.getCurrentSession().load(UserTest.class, id);
        if (userTest != null) {
            this.sessionFactory.getCurrentSession().delete(userTest);
        }
    }

    @Override
    public List<UserTest> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from UserTest").list();
    }

    @Override
    public UserTest update(UserTest userTest) {
        sessionFactory.getCurrentSession().update(userTest);
        return userTest;
    }

    @Override
    public UserTest get(int id) {
        return (UserTest) sessionFactory.getCurrentSession().get(UserTest.class, id);
    }
}
