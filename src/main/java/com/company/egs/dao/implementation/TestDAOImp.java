package com.company.egs.dao.implementation;

import com.company.egs.dao.interfaces.TestDAO;
import com.company.egs.entity.Test;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TestDAOImp implements TestDAO {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public void add(Test test) {
        sessionFactory.getCurrentSession().saveOrUpdate(test);
    }

    @Override
    public void delete(int id) {
        Test test = (Test) sessionFactory.getCurrentSession().load(Test.class, id);
        if (test != null) {
            this.sessionFactory.getCurrentSession().delete(test);
        }
    }

    @Override
    public List<Test> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Test").list();
    }

    @Override
    public Test update(Test test) {
        sessionFactory.getCurrentSession().update(test);
        return test;
    }

    @Override
    public Test get(int id) {
        return (Test) sessionFactory.getCurrentSession().get(Test.class, id);
    }
}
