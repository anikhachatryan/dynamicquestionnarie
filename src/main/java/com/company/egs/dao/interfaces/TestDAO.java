package com.company.egs.dao.interfaces;


import com.company.egs.entity.Test;

import java.util.List;

public interface TestDAO {
    void add(Test test);

    void delete(int id);

    List<Test> getAll();

    Test update(Test test);

    Test get(int id);
}
