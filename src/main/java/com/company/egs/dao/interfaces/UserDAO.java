package com.company.egs.dao.interfaces;

import com.company.egs.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface UserDAO extends Repository<User, String> {

    void add(User user);

    void delete(int id);

    List<User> getAll();

    User update(User user);

    User get(int id);

    @Query("SELECT id FROM User WHERE email = :email")
    int get(String email);
}
