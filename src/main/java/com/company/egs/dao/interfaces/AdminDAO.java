package com.company.egs.dao.interfaces;

import com.company.egs.entity.Admin;

import java.util.List;

public interface AdminDAO {
    void add(Admin admin);

    void delete(int id);

    List<Admin> getAll();

    Admin update(Admin admin);

    Admin get(int id);
}
