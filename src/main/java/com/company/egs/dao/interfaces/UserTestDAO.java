package com.company.egs.dao.interfaces;

import com.company.egs.entity.UserTest;

import java.util.List;

public interface UserTestDAO {

    void add(UserTest userTest);

    void delete(int id);

    List<UserTest> getAll();

    UserTest update(UserTest userTest);

    UserTest get(int id);
}
